from tkinter import *
from PIL import Image, ImageTk
import tkinter.messagebox as MessageBox


login = Tk()
login.geometry("800x500+565+270")
login.title("Sistema de Login")

text_login = Label(login, text="Área de Acesso Restrito", font=("Arial", 20), fg="#3b3b3b")
text_login.place(x=240, y=50)

img_usuario = Image.open('imagens/username.png')
img_usuario = img_usuario.resize((35, 35))
img_usuario = ImageTk.PhotoImage(img_usuario)

label_img_usuario = Label(login, image=img_usuario, compound=LEFT, font=('Ivy 10 bold'), anchor='nw')
label_img_usuario.place(x=240, y=145)

img_senha = Image.open('imagens/password.png')
img_senha = img_senha.resize((35, 35))
img_senha = ImageTk.PhotoImage(img_senha)

label_img_senha = Label(login, image=img_senha, compound=LEFT, font=('Ivy 10 bold'), anchor='nw')
label_img_senha.place(x=240, y=215)

campo_usuario = Entry(login, font=10, width=20)
campo_usuario.place(x=300, y=150)

campo_senha = Entry(login, font=10, width=20, show="*")
campo_senha.place(x=300, y=215)

botao_entrar = Button(login, text="Entrar no sistema")
botao_entrar.place(x=310, y=300)

login.mainloop()
